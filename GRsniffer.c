#include <stdio.h>

/*
 * main.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pcap.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>
#include <endian.h>
#include <net/if_arp.h>
#include <linux/ip.h>
#include <linux/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <net/if.h>
#include <pcap.h>

#define IFACE_MAX_SIZE 20

uint8_t mac_local[6];

int ContadorPacotes = 0;

static void print_usage(int argc, char** argv) {
    fprintf(stderr, "Usage: %s [-s <n skip packets>] [-m <max packets>] [-i <iface>] | [-f <file>] \n", argv[0]);
}

static void decode(uint8_t* arg, const struct pcap_pkthdr* pkthdr, const uint8_t* packet){ 

	uint8_t Controle[2];
	uint8_t vSalto[2];
	uint8_t ProtocoloRede[2];
	uint8_t ProtocoloTransporte[1];
	uint8_t Portas[4];
	uint16_t Salto;
	uint8_t tipo;
	uint8_t subTipo;
	uint8_t doAPparaAP;
	char* strtipo;
	char* strsubtipo;
	char* strProtocoloRede = "";
	char* strProtocoloTransporte = "";
	char* strPortaOrigem = "";
	char* strPortaDestino = "";
	packet += 2;

	ContadorPacotes += 1;

	memcpy(vSalto, packet, 2); /* pegando tamanho do salto até o início do quadro 802.11 */

	Salto = vSalto[0] + ((uint16_t)vSalto[1]); //transformando o saldo em inteiro

	packet += (Salto-2); //Saltando para início do cabeçalho 802.11

	memcpy(Controle, packet, 2); /* Pegando a parte de controle do cabeçalho 802.11*/

	//pegando tipo e subtipo do pacote
	tipo = Controle[0]<< 4;
	tipo = tipo >> 6;
	subTipo = Controle[0]>> 4;


	//veificando quantos endereços estão presentes no pacote (com os campos "do AP" e "para o AP")
	doAPparaAP = Controle[1] << 6;
	doAPparaAP = doAPparaAP >> 6;

	if (tipo == 0) 
	{	
		strtipo = "Management";

		//Pegando Descrição de subtipo		
		if (subTipo == 0) {
			strsubtipo = "Association Request";
		} else if (subTipo == 1){
			strsubtipo = "Association Response";
		} else if (subTipo == 2){ 
			strsubtipo = "Reassociation Request";
		} else if (subTipo == 3){ 
			strsubtipo = "Reassociation Responce";
		} else if (subTipo == 4){ 
			strsubtipo = "Probe Request";
		} else if (subTipo == 5){ 
			strsubtipo = "Probe Response";
		} else if (subTipo == 8){ 
			strsubtipo = "Beacon";
		} else if (subTipo == 9){ 
			strsubtipo = "ATIM";
		} else if (subTipo == 10){ 
			strsubtipo = "Desassociation";
		} else if (subTipo == 11) {
			strsubtipo = "Authentication";
		} else if (subTipo == 12) {
			strsubtipo = "Deauthentication";
		} else if (subTipo == 13) {
			strsubtipo = "Action";
		} else{
			strsubtipo = "Reservado";
		    }
	}
	else if (tipo == 1) 
	{
		strtipo = "Controll";

		//Pegando Descrição de subtipo
		if (subTipo == 8) {
			strsubtipo = "Block Ack Request";
		} else if (subTipo == 9) {
			strsubtipo = "Block Ack";
		} else if (subTipo == 10) {
			strsubtipo = "PS-Poll";
		} else if (subTipo == 11) {
			strsubtipo = "RTS";
		} else if (subTipo == 12) {
			strsubtipo = "CTS";
		} else if (subTipo == 13) {
			strsubtipo = "ACK";
		} else if (subTipo == 14) {
			strsubtipo = "CF END";
		} else if (subTipo==15) {
			strsubtipo = "CF END + CF ACK";
		} else{
			strsubtipo = "Erro ao classificar subtipo";
		}
	
	}
	else if (tipo == 2) 
	{ 
		strtipo = "Data";
		
		//Saltando o cabeçalho 802.11
		if (doAPparaAP == 0 || doAPparaAP == 1 ||  doAPparaAP == 2)
		{
			packet += 24;
		} else
		{
			packet += 30;
		}


		//Pegando Descrição de subtipo
		if (subTipo == 0) {
			strsubtipo = "Data";
		} else if (subTipo==1) {
			strsubtipo = "Data + CF-Ack";
		} else if (subTipo==2) {
			strsubtipo = "Data + CF-Poll";
		} else if (subTipo==3) {
			strsubtipo = "Data + CF-Ack + CF-Poll";
		} else if (subTipo==4) {
			strsubtipo = "Null (no data)";
		} else if (subTipo==5) {
			strsubtipo = "CF-Ack (no data)";
		} else if (subTipo==6) {
			strsubtipo = "CF-Poll (no data)";
		} else if (subTipo==7) {
			strsubtipo = "CF-Ack + CF-Poll (no data)";
		} else if (subTipo==8) {
			strsubtipo = "QoS Data";
			//QOS
			packet += 2;
		} else if (subTipo==9) {
			strsubtipo = "QoS Data + CF-Ack";
			//QOS
			packet += 2;
		} else if (subTipo==10) {
			strsubtipo = "QoS Data + CF-Poll";
			//QOS
			packet += 2;
		} else if (subTipo==11) {
			strsubtipo = "QoS Data + CF-Ack + CF-Poll";
			//QOS
			packet += 2;
		} else if (subTipo==12) {
			strsubtipo = "QoS Null (no data)";
			//QOS
			packet += 2;
		} else if (subTipo==14) {
			strsubtipo = "QoS CF-Poll (no data)";
			//QOS
			packet += 2;
		} else if (subTipo==15) {
			strsubtipo = "QoS CF-Ack + CF-Poll (no data)";
			//QOS
			packet += 2;
		} else{
			strsubtipo = "Reserved";
		}

		
		//Saltando Mesh Control
		packet += 6;
		//Avançado LLC para pegar o próximo protocolo
		packet += 6;
		
		memcpy(ProtocoloRede, packet, 2); // pegando o código do protocolo de rede

		if (ProtocoloRede[0] == 8 & ProtocoloRede[1] == 0)
		{
			strProtocoloRede = "Protocolo de rede: IP";
			packet += 11;//pulando para o protocolo de transporte
		
			memcpy(ProtocoloTransporte, packet, 1); // pegando o código do protocolo de transporte

			if (ProtocoloTransporte[0] == 1)
			{
				strProtocoloTransporte = "Protocolo de transporte: ICMP";
			} else if (ProtocoloTransporte[0] == 6)
			{
				strProtocoloTransporte = "Protocolo de transporte: TCP";
				
				packet += 24;//pulando cabeçalho IP

				memcpy(Portas, packet, 4);//pegando porta de origem e destino
				
				sprintf(strPortaOrigem, "Porta de Origem: %d", Portas[0],Portas[1]);
				sprintf(strPortaDestino, "Porta de Destino: %d", Portas[2],Portas[3]);				
				
			} else if (ProtocoloTransporte[0] == 17)
			{
				strProtocoloTransporte = "Protocolo de transporte: UDP";
				packet += 24;//pulando cabeçalho IP

				memcpy(Portas, packet, 4);//pegando porta de origem e destino
				
				sprintf(strPortaOrigem, "Porta de Origem: %d", Portas[0],Portas[1]);
				sprintf(strPortaDestino, "Porta de Destino: %d", Portas[2],Portas[3]);				
				
				
			} else if (ProtocoloTransporte[0] == 2)
			{
				strProtocoloTransporte = "Protocolo de transporte: IGMP";
			}
			
		}else if (ProtocoloRede[0] == 8 & ProtocoloRede[1] == 6)
		{
			strProtocoloRede = "Protocolo de rede: ARP";
		}else if (ProtocoloRede[0] == 80 & ProtocoloRede[1] == 35)
		{
			strProtocoloRede = "Protocolo de rede: RARP";
		}else if (ProtocoloRede[0] == 8 & ProtocoloRede[1] == 8)
		{
			strProtocoloRede = "Protocolo de rede: Frame Relay ARP";
		}else if (ProtocoloRede[0] == 88 & ProtocoloRede[1] == 47)
		{
			strProtocoloRede = "Protocolo de rede: MPLS";
		}else if (ProtocoloRede[0] == 88 & ProtocoloRede[1] == 48)
		{
			strProtocoloRede = "Protocolo de rede: MPLS with upstream-assigned label";
		}else if (ProtocoloRede[0] == 88 & ProtocoloRede[1] == 61)
		{
			strProtocoloRede = "Protocolo de rede: MCAP";
		}else if (ProtocoloRede[0] == 88 & ProtocoloRede[1] == 63)
		{
			strProtocoloRede = "Protocolo de rede: PPP over Ethernet (PPPoE) Discovery Stage";
		}else if (ProtocoloRede[0] == 88 & ProtocoloRede[1] == 64)
		{
			strProtocoloRede = "Protocolo de rede: (PPPoE) Session Stage";
		}else if (ProtocoloRede[0] == 89 & ProtocoloRede[1] == 46)
		{
			strProtocoloRede = "Protocolo de rede: TRILL RBridge Channel";
		}else
		{
			strProtocoloRede = "outros";
		}



	} else if (tipo == 3) 
	{ 
		strtipo = "Reservado";
		strsubtipo = "Reservado";
	} else{
		strtipo = "Erro na identificação de tipo";
		strsubtipo = "";
	}



	printf("%d - Tipo = %s SubTipo = %s %s %s %s %s \n", 
		ContadorPacotes, strtipo, strsubtipo, strProtocoloRede, strProtocoloTransporte, strPortaOrigem, strPortaDestino);


    return; 
} 


int main(int argc, char** argv)
{
    struct ifreq ifr;  // Get MAC address
    struct sockaddr *p;  //hw addr
    char file[200];
    int fd=socket(AF_UNIX,SOCK_DGRAM,0);
    if (fd==-1) {
    	printf("Erro!\n");
	exit(1);
    }

    char dev[IFACE_MAX_SIZE] = { 0 } ;

    /*----*/

    pcap_t *pool;

    int opt, ret;

    int max_pkts = -1;
    int skip_pkts = 0;

    while ((opt = getopt(argc, argv, "i:f:m:s:")) != -1) {
        switch(opt) {
            case 'i':
                strncpy(dev, optarg, IFACE_MAX_SIZE - 1);
                break;
            case 'f':
                strncpy(file, optarg, IFACE_MAX_SIZE - 1);
                break;
            case 'm':
                max_pkts = atoi(optarg); 
                if (max_pkts < 0) {
                    max_pkts = -1;
                }
                break;
            case 's':
                skip_pkts = atoi(optarg); 
                if (skip_pkts < 0) {
                    skip_pkts = 0;
                }
                break;
            default:
                print_usage(argc, argv);
                exit(EXIT_FAILURE);
        }
    }
    
    if ((!strcmp(dev, "") && !strcmp(file, "")) || (strcmp(dev, "") && strcmp(file, "") )) {
        print_usage(argc, argv);
        exit(EXIT_FAILURE);
    }

    if (strcmp(dev, "")) {
	/* get MAC addresss */
        memset(&ifr, 0, sizeof(ifr));
        strcpy(ifr.ifr_name, dev);
        if (ioctl(fd, SIOCGIFINDEX, &ifr) < 0) {
                //syslog(LOG_INFO|LOG_LOCAL2,"Interface desconhecida= %s", dev );
                exit(2);
        }
        //ifindex = ifr.ifr_ifindex;

        if (ioctl(fd, SIOCGIFHWADDR, &ifr) < 0) {
                //syslog(LOG_INFO|LOG_LOCAL2,"Interface desconhecida= %s", dev );
                exit(2);
        }
        p = (struct sockaddr *) &ifr.ifr_hwaddr;
        memcpy(mac_local, p->sa_data, 6);
        fprintf(stderr, "mac de %s  = %02x:%02x:%02x:%02x:%02x:%02x\n", dev,
           mac_local[0], mac_local[1], mac_local[2], 
           mac_local[3], mac_local[4], mac_local[5]);
        /*----------------------------*/

        char errbuf[PCAP_ERRBUF_SIZE];
        
        pool = pcap_create(dev, errbuf);
        if(pool == NULL) {
            fprintf(stderr, "Something goes wrong while openning [%s]: %s\n", dev, errbuf);
            exit(EXIT_FAILURE);
        }   
        ret = pcap_activate(pool);
        if (ret != 0) {
            fprintf(stderr, "Something goes wrong while activating [%s]: %s\n", dev, pcap_geterr(pool));
            exit(EXIT_FAILURE);
        }      

    } else if (strcmp(file, "")) {
        char errbuf[PCAP_ERRBUF_SIZE];
        //fprintf(stderr, "File support is not implemented yet\n");

        pool = pcap_open_offline(file, errbuf);
        if (pool == NULL) {
            fprintf(stderr, "Something goes wrong while openning '%s': %s\n", file, errbuf);
            exit(EXIT_FAILURE);
        }
    } else {
        fprintf(stderr, "Desconhecido \n");
        perror("I'm not supposed to catch here");
        abort();
    }
    
    ret = pcap_datalink(pool);
    if(ret != DLT_IEEE802_11_RADIO) {
        fprintf(stderr, "[%s] is not a DLT_IEEE802_11_RADIO interface\n", dev);
        exit(EXIT_FAILURE);
    }
   
    if (max_pkts && pcap_loop(pool, -1, decode, NULL) == -1){
        fprintf(stderr, "ERROR: %s\n", pcap_geterr(pool) );
        exit(EXIT_FAILURE);
    }
    
    close(fd);
    return (0);
}


